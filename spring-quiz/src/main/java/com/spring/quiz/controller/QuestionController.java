package com.spring.quiz.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.spring.quiz.models.entity.Answer;
import com.spring.quiz.models.service.IQuestionService;

@Controller
@RequestMapping("/question")
public class QuestionController {

	@Autowired
	private IQuestionService questionService;

	@GetMapping("/")
	public String getQuestions(Model model) {
		model.addAttribute("title", "Questions");
		model.addAttribute("titleQuestion", "Please, answer the following questions");
		Answer answer = new Answer();
		answer.setQuestions(questionService.getAllquestions());
		model.addAttribute("form", answer);
		return "question";
	}

	@PostMapping("/")
	public String saveQuestions(@Valid @ModelAttribute("questions") Answer questions, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			model.addAttribute("title", "Questions");
			model.addAttribute("titleQuestion", "Opps, There was a error, please check the filds");
			model.addAttribute("form", questions);
			return "question";
		}

		boolean isSend = questionService.sendToAPI(questions);

		return "redirect:/question/";
	}

}
