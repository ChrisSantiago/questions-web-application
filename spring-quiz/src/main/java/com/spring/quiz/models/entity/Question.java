package com.spring.quiz.models.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "questions")
public class Question implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer questionId;

	@Column(name = "question_text")
	private String question;

	@NotEmpty
	@Column(name = "asnwer")
	private String asnwer;

	public Question() {

	}

	public Question(Integer questionId, String question, String asnwer) {
		this.questionId = questionId;
		this.question = question;
		this.asnwer = asnwer;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getAsnwer() {
		return asnwer;
	}

	public void setAsnwer(String asnwer) {
		this.asnwer = asnwer;
	}

	@Override
	public String toString() {
		return "Question [questionId=" + questionId + ", question=" + question + ", asnwer=" + asnwer + "]";
	}

}
