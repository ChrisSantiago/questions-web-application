package com.spring.quiz.models.service;

import java.util.List;

import com.spring.quiz.models.entity.Answer;
import com.spring.quiz.models.entity.Question;

public interface IQuestionService {

	public List<Question> getAllquestions();
	
	public Boolean sendToAPI(Answer request);

}
