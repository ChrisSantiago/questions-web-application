package com.spring.quiz.models.service;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.spring.quiz.models.dao.IQuestionDao;
import com.spring.quiz.models.entity.Answer;
import com.spring.quiz.models.entity.Question;

@Service
public class QuestionServiceImpl implements IQuestionService {

	@Autowired
	private IQuestionDao questionDao;

	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@Transactional(readOnly = true)
	@Override
	public List<Question> getAllquestions() {
		return questionDao.getAllquestions();
	}

	@Override
	public Boolean sendToAPI(Answer request) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<Answer> entity = new HttpEntity<Answer>(request, headers);
			ResponseEntity<String> response = restTemplate.exchange("https://demo2139500.mockable.io/questions ",
					HttpMethod.POST, entity, String.class);
			if (response.getStatusCode().value() == 200) {
				LOG.info("IT WAS SENT");
				return true;
			}

		} catch (RestClientException e) {
			LOG.info("ERROR: " + e);
		}
		return false;
	}

}
