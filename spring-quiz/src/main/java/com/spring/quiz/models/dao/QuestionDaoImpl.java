package com.spring.quiz.models.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.spring.quiz.models.entity.Question;

@Repository
public class QuestionDaoImpl implements IQuestionDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Question> getAllquestions() {
		List<Question> questions = entityManager.createNativeQuery(" SELECT * FROM questions", Question.class)
				.getResultList();
		return questions;
	}

}
