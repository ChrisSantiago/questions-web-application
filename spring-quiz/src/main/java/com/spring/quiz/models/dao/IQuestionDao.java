package com.spring.quiz.models.dao;

import java.util.List;

import com.spring.quiz.models.entity.Question;

public interface IQuestionDao {

	public List<Question> getAllquestions();
}
