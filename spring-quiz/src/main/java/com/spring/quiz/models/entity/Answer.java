package com.spring.quiz.models.entity;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

public class Answer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Valid
	List<Question> questions;

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Answer [questions=" + questions + "]";
	}

}
